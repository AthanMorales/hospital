/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Mauricio
 */
public class Hospital {
    public static void main(String[] args) {
        Date date = new Date();
        Patient p1 = new Patient("Athan",date,"Will");
        
        List<Patient> patients = new ArrayList<Patient>();
        patients.add(p1);
        String time = "90min";

        ResearchGroup rg = new ResearchGroup(patients,time);
        
        List<Patient> gP = rg.getGroupPatiens();
        for (Patient p : gP) {
            System.out.println("Name: " + p.getDoctor() + "and Doctor: " + p.getDoctor());
        }
    }
}
