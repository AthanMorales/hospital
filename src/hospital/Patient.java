/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.Date;

/**
 *
 * @author Mauricio
 */
public class Patient {

    private String name;
    private Date dateOfBirth;
    private String doctor;

    public String getName() {
        return this.name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return this.dateOfBirth;
    }

    /**
     *
     * @param dateOfBirth
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDoctor() {
        return this.doctor;
    }

    /**
     *
     * @param doctor
     */
    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public Patient(String name, Date dateOfBirth, String doctor) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.doctor = doctor;
    }

}
