package hospital;

import java.util.List;

public class WristBand {

 private String barCode;
 private List<Patient> patiens;

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public List<Patient> getPatiens() {
        return patiens;
    }

    public void setPatiens(List<Patient> patiens) {
        this.patiens = patiens;
    }

    public WristBand(String barCode, List<Patient> patiens) {
        this.barCode = barCode;
        this.patiens = patiens;
    }
}