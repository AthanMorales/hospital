package hospital;

import java.util.List;

public class ResearchGroup {

 private final List<Patient> patients;
 private String time;

    public ResearchGroup(List<Patient> patients, String time) {
        this.patients = patients;
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
    public List<Patient> getGroupPatiens(){
        return this.patients;
    }
}